(defsystem cl-logo
  :version "0.0.1"
  :author "Hirotetsu Hongo"
  :depends-on (alexandria cl-ppcre optima split-sequence)
  :components ((:file "package")
               (:file "util" :depends-on ("package"))
               (:file "parse" :depends-on ("package" "util"))
               (:file "core" :depends-on ("package" "util"))
               (:file "logo" :depends-on ("package" "util" "parse" "core"))))
