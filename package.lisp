(defpackage cl-logo
  (:nicknames :logo)
  (:use :cl)
  (:import-from :alexandria)
  (:import-from :ppcre)
  (:import-from :split-sequence
                #:split-sequence)
  (:import-from :optima
                #:match
                )
  (:export )
  )
